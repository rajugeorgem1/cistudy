FROM php:7.4-apache

RUN apt-get update -y

RUN apt-get install -y \
	git \
	zip \
	unzip

RUN docker-php-ext-install mysqli \
	&& docker-php-ext-enable mysqli

# Update Php Settings
COPY custom.ini $PHP_INI_DIR/conf.d

WORKDIR /var/www/cistudy

COPY custom.conf /etc/apache2/sites-available/custom.conf

RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf \
    && a2enmod rewrite \
    && a2dissite 000-default \
    && a2ensite custom \
    && service apache2 restart


COPY --from=composer /usr/bin/composer /usr/bin/composer
COPY composer.json composer.json
RUN composer install

COPY . .

EXPOSE 80

RUN chown -R www-data:www-data /var/www



