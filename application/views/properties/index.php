<div id="container">
	<h1>Welcome to <?php echo $user_name; ?></h1>
	<?php echo $selected_filter ?>
	<h2>Please select from below</h2>
	<form method="get" action="<?php echo site_url('properties/set_filter'); ?>">
	<select name="filter" id="">
		<?php foreach ($status_group as $role) { ?>
			<option><?php echo $role; ?></option>
		<?php } ?>
	</select>
	<input class="button warning" type="submit" value="Select" />
		</form>


	<div class="row column">
  <h3>Properties details</h3>
  <table border="0">
    <tr>
      <td>IMAGE</td>
      <td>NAME</td>
      <td>LOCATION</td>
      <td>STATUS</td>  
      <td>ACTION</td>  
    </tr>
	<?php foreach($properties as $property ) { ?>
    <tr>
      <td><img src="<?php echo base_url("assets/images/{$property['image']}"); ?>" width="150" /></td>  
      <td><?php echo $property['name'] ?> </td>
      <td><?php echo $property['city'] ?>, <?php echo $property['state'] ?> </td>
      <td>Available</td>
      <td>
        <a href="<?php echo site_url("properties/show/{$property['id']}"); ?>" class="button success">View Details</a>
        <a href="<?php echo site_url("properties/edit/{$property['id']}"); ?>" class="button">Edit Details</a>
      </td>  
    </tr>
	<?php } ?>
  </table>
  <br/>
</div>
